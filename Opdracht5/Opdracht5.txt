Studentnummer:332002
Naam: Brandon

Verschilt de Terminal in Visual Studio Code met Git Bash?
	# Nee

Waar worden Branches vaak voor gebruikt?
	# Om aan features en bugfixes te kunnen werken

Hoe vaak ben je in Opdracht 5A van Branch gewisseld?
	# 2

Vul de volgende commando's aan:
 -Checken op welke branch je aan het werken bent:
	# git branch
 -Nieuwe Branch aanmaken
	# git branch <naam van nieuwe branch>
 -Van Branch wisselen
	# git checkout <naam van branch>
 -Branch verwijderen
	# git branch -d <branch>