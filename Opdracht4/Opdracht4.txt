Studentnummer:332002
Naam: Brandon

Hoe open je de Terminal in Visual Studio Code?
	# Boven aan "Terminal" en dan "New Terminal" 

Met welk commando kan je checken of welke bestanden zijn toegevoegd aan de commit en welke niet?
	# git status

Wat is het commando van een multiline commit message?
	# commit -m

Hoeveel commando's heb je in opdracht 4a uitgevoerd?
	# 9

Zoek het volgende commando op:
 - 1 commit teruggaan in de commit history. (reset)
	# git reset HEAD-1
	# git reset --soft/--mixed/--hard HEAD-2
	# git revert HEAD-3
	
